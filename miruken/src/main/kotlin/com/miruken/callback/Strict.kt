package com.miruken.callback

@Target(AnnotationTarget.CLASS,AnnotationTarget.FUNCTION,
        AnnotationTarget.TYPE,AnnotationTarget.PROPERTY_GETTER)
annotation class Strict